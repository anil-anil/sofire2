# cmake_minimum_required(VERSION 3.6) #maybe ok?
cmake_minimum_required(VERSION 3.17)

# Append local CMake module directory
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# Set package metadata
set(CPACK_PACKAGE_NAME "SOFIRE")
set(CPACK_PACKAGE_VERSION_MAJOR "2")
set(CPACK_PACKAGE_VERSION_MINOR "1")
set(CPACK_PACKAGE_VERSION_PATCH "1")
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Sodium pool fire simulation")
set(CPACK_PACKAGE_VENDOR "Acorvid Technical Services Corporation")
set(CPACK_PACKAGE_CONTACT "Bob Apthorpe <bob.apthorpe@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/apthorpe/sofire2")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
# RAA: What is this? Base installation directory for NSIS installer?
set(CPACK_PACKAGE_INSTALL_DIRECTORY "${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}")
#? set(CPACK_PACKAGE_INSTALL_DIRECTORY "${CPACK_PACKAGE_NAME}")
# set(CPACK_SET_DESTDIR true)
# set(CPACK_INSTALL_PREFIX /opt/${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION})

# Show your work...
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set project name and language
project(SOFIRE2
        LANGUAGES Fortran
        VERSION "${CPACK_PACKAGE_VERSION}")

find_package(Doxygen REQUIRED dot)
find_package(Gnuplot)
find_package(LATEX COMPONENTS PDFLATEX MAKEINDEX BIBTEX)

# Set default installation paths; should be invoked after setting project language(s)
include(GNUInstallDirs)

# CTest setup
# Needed for valgrind (usually only enable_testing() is needed)
include(CTest)
enable_testing()

# Set up Doxygen and custom LaTeX content and templates
set(USERDOC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/userdoc")
list(APPEND L_DOXYGEN_CITE_BIB_FILES ${USERDOC_DIR}/sofire2.bib)
list(REMOVE_DUPLICATES L_DOXYGEN_CITE_BIB_FILES)

list(APPEND L_DOXYGEN_LATEX_EXTRA_FILES
    ${USERDOC_DIR}/siunitx_us_customary.tex
    ${USERDOC_DIR}/manual_metadata.tex
    ${USERDOC_DIR}/overview.tex
    ${USERDOC_DIR}/fig1_one_cell_geometry.png
    ${USERDOC_DIR}/fig2_two_cell_geometry.png
    ${USERDOC_DIR}/theory.tex
    ${USERDOC_DIR}/c1_geometry.png
    ${USERDOC_DIR}/c2_geometry.png
    ${USERDOC_DIR}/code_operation.tex
    ${USERDOC_DIR}/benchmark.tex
    ${USERDOC_DIR}/large_sodium_fires_modeling_experiment_diagram_1.png
    ${USERDOC_DIR}/C2_LTV_schematic.png
    ${USERDOC_DIR}/C1B4.INP
    ${USERDOC_DIR}/C1B5.INP
    ${USERDOC_DIR}/C1B6.INP
    ${USERDOC_DIR}/fig3_test_4_pressure_benchmark.png
    ${USERDOC_DIR}/fig4_test_4_burn_rate_benchmark.png
    ${USERDOC_DIR}/fig5_test_5_pressure_benchmark.png
    ${USERDOC_DIR}/fig6_test_5_burn_rate_benchmark.png
    ${USERDOC_DIR}/fig7_test_6_pressure_benchmark.png
    ${USERDOC_DIR}/fig8_test_6_pool_temp_benchmark.png
    ${USERDOC_DIR}/fig9_c2_test_2_pressure_benchmark.png
    ${USERDOC_DIR}/fig10_c2_test_2_ox_fraction_benchmark.png
    ${USERDOC_DIR}/C1_benchmark_1.pdf
    ${USERDOC_DIR}/C1_benchmark_2.pdf
    ${USERDOC_DIR}/C1_benchmark_3.pdf
    ${USERDOC_DIR}/C1_benchmark_4.pdf
    ${USERDOC_DIR}/C1_benchmark_5.pdf
    ${USERDOC_DIR}/C1_benchmark_6.pdf
    ${USERDOC_DIR}/sofire_1c.jcl
    ${USERDOC_DIR}/input_description.tex
    ${USERDOC_DIR}/c1_input_data_card.png
    ${USERDOC_DIR}/c2_input_data_card.png
    ${USERDOC_DIR}/output_description.tex
    ${USERDOC_DIR}/error_reports.tex
    ${USERDOC_DIR}/e20200817-1.tex
    ${L_DOXYGEN_CITE_BIB_FILES}
)
list(REMOVE_DUPLICATES L_DOXYGEN_LATEX_EXTRA_FILES)

include(SetupDoxygen)

###############################################################################
## Build ######################################################################
###############################################################################

include(DefineLegacyFortranOptions)

list(APPEND FCOPTS
    ${FCOPT_NO_OPTIMIZATION}
    ${FCOPT_WALL}
    ${FCOPT_FCHECKALL}
    ${FCOPT_DEBUG}
    ${FCOPT_BACKTRACE}
)

# Set recent language standard if available
if(${FC_ALLOWS_STD_F2018})
    list(APPEND FCOPTS ${FCOPT_STD_F2018})
elseif(${FC_ALLOWS_STD_F2008})
    list(APPEND FCOPTS ${FCOPT_STD_F2008})
endif()

message(STATUS "Fortran compiler options set to ${FCOPTS}")

# Target sofire2_1c: SOFIRE2 one-cell model
set(SOFIRE2_1C_BASE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/sofire2_1c")
set(SOFIRE2_1C_SOURCE_DIR "${SOFIRE2_1C_BASE_DIR}/src")

# Define sources for SOFIRE2_1C executable
set(SOFIRE2_1C_SOURCES
    "${SOFIRE2_1C_SOURCE_DIR}/m_constant.f90"
    "${SOFIRE2_1C_SOURCE_DIR}/m_dataframe.f90"
    "${SOFIRE2_1C_SOURCE_DIR}/m_input.f90"
    "${SOFIRE2_1C_SOURCE_DIR}/m_output.f90"
    "${SOFIRE2_1C_SOURCE_DIR}/sofire2_1c.f90"
    "${SOFIRE2_1C_SOURCE_DIR}/sofire2_param.f90"
)

# Define SOFIRE2 one-cell executable artifact
add_executable(sofire2_1c ${SOFIRE2_1C_SOURCES})

target_compile_options(sofire2_1c PUBLIC ${FCOPTS})

install(TARGETS sofire2_1c)

# Target sofire2_2c: SOFIRE2 two-cell model - TBD
# set(SOFIRE2_2C_BASE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/sofire2_2c")
# set(SOFIRE2_2C_SOURCE_DIR "${SOFIRE2_2C_BASE_DIR}/src")

# Define sources for SOFIRE2_2C executable
# set(SOFIRE2_2C_SOURCES
#     "${SOFIRE2_2C_SOURCE_DIR}/sofire2_2c.f"
# )

# Define SOFIRE2 two-cell executable artifact
# add_executable(sofire2_2c
#     ${SOFIRE2_2C_SOURCES}
# )

# target_compile_options(sofire2_2c PUBLIC ${FCOPTS})

# install(TARGETS sofire2_2c)

###############################################################################
## Testing ####################################################################
###############################################################################
set(TEST_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/test")

# Create C1 test output as post-build action
set(SOFIRE2_1C_TEST_DIR "${TEST_BASE_DIR}/sofire2_1c")
add_custom_command(TARGET sofire2_1c POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E echo "Copying test/c1" 
    COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_CURRENT_SOURCE_DIR}/test/c1" "${SOFIRE2_1C_TEST_DIR}"
    COMMAND ${CMAKE_COMMAND} -E echo "Generating C1.OUT"
    COMMAND $<TARGET_FILE:sofire2_1c> < "${SOFIRE2_1C_TEST_DIR}/C1.INP" > "${SOFIRE2_1C_TEST_DIR}/C1.OUT"
    COMMAND ${CMAKE_COMMAND} -E rename "${CMAKE_CURRENT_BINARY_DIR}/sofire2_results.csv" "${SOFIRE2_1C_TEST_DIR}/C1_results.csv"
    BYPRODUCTS "${SOFIRE2_1C_TEST_DIR}/C1.OUT" "${SOFIRE2_1C_TEST_DIR}/C1_results.csv"
    USES_TERMINAL
)

# Tests using CMake's simple built-in "compare_files" command
# Compare output and reformatted original reference data
add_test(NAME C1_original
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1.OUT C1_reformat_REF.OUT
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example output - truncated run
add_test(NAME C1_20200809_baseline_out
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1.OUT C1_20200809_REF.OUT
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example csv results - truncated run
add_test(NAME C1_20200809_baseline_csv
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1_results.csv C1_20200809_REF.csv
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example output - full run
add_test(NAME C1_20200815_baseline_out
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1.OUT C1_20200815_REF.OUT
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example csv results - full run
add_test(NAME C1_20200815_baseline_csv
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1_results.csv C1_20200815_REF.csv
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example output - ZF2 regression
# CSV results are identical; only DELF3 and DELF4 change in C1.OUT
add_test(NAME C1_20200817_baseline_out
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1.OUT C1_20200817_REF.OUT
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example output - double precision, tabular output
add_test(NAME C1_20200817_double_out
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1.OUT C1_20200817_DP_REF.OUT
    CONFIGURATIONS Debug Release ""
)

# Compare output and unverified recent example output - double precision, tabular output
add_test(NAME C1_20200817_double_csv
    COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol C1_results.csv C1_20200817_DP_REF.csv
    CONFIGURATIONS Debug Release ""
)

set_tests_properties(
    C1_original
    C1_20200809_baseline_out
    C1_20200809_baseline_csv
    C1_20200815_baseline_out
    C1_20200815_baseline_csv
    C1_20200817_baseline_out
    C1_20200817_double_out
    C1_20200817_double_csv
    PROPERTIES
    WORKING_DIRECTORY "${SOFIRE2_1C_TEST_DIR}"
    DEPENDS sofire2_1c
    TIMEOUT 30
)

set_tests_properties(
    C1_original
    C1_20200809_baseline_out
    C1_20200809_baseline_csv
    C1_20200815_baseline_out
    C1_20200815_baseline_csv
    C1_20200817_baseline_out
    PROPERTIES
    WILL_FAIL TRUE
)

# # Create C2 test output as post-build action
# set(SOFIRE2_2C_TEST_DIR "${TEST_BASE_DIR}/sofire2_2c")
# add_custom_command(TARGET sofire2_2c POST_BUILD
#     COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_CURRENT_SOURCE_DIR}/test/c2" "${SOFIRE2_2C_TEST_DIR}"
#     COMMAND $<TARGET_FILE:sofire2_2c> < "${SOFIRE2_2C_TEST_DIR}/C2.INP" > "${SOFIRE2_2C_TEST_DIR}/C2.OUT"
#     BYPRODUCTS "${SOFIRE2_2C_TEST_DIR}/C2.OUT"
#     USES_TERMINAL
# )

# # Compare output and reference data with CMake's simple built-in "compare_files" command
# add_test(NAME C2
#     COMMAND ${CMAKE_COMMAND} -E compare_files --ignore-eol "C2.OUT" "C2_REF.OUT"
#     CONFIGURATIONS Debug Release ""
#     WORKING_DIR "${SOFIRE2_2C_TEST_DIR}"
# )


###############################################################################
## Analysis ###################################################################
###############################################################################

###############################################################################
## Packaging ##################################################################
###############################################################################

# Documentation files
install(FILES LICENSE README.md
        # refs/SOFIRE_II_User_Report.pdf
        # refs/ANL-ARC-250.pdf
        DESTINATION doc)

# One-cell test files
install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/test/c1"
        DESTINATION test
        FILES_MATCHING
        PATTERN *.csv
        PATTERN *.INP
        PATTERN *_REF.OUT
)

# Two-cell test files
install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/test/c2"
        DESTINATION test
        FILES_MATCHING
        PATTERN *.csv
        PATTERN *.INP
        PATTERN *_REF.OUT
)

list(APPEND CPACK_GENERATOR ZIP)

if(WIN32)
    # Set up NSIS
    find_package(NSIS)
    if(NSIS_FOUND)
        # set(CPACK_NSIS_DISPLAY_NAME "${GRAPHVIZ_VERSION_FULL}")
        # set(CPACK_NSIS_PACKAGE_NAME "${GRAPHVIZ_VERSION_FULL}")
        set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}/img/SOFIRE2.ico")
        set(CPACK_NSIS_MUI_UNIICON "${CMAKE_CURRENT_SOURCE_DIR}/img/SOFIRE2.ico")
        set(CPACK_NSIS_INSTALLED_ICON_NAME "Uninstall.exe")
        set(CPACK_NSIS_HELP_LINK "${CPACK_PACKAGE_HOMEPAGE_URL}")
        set(CPACK_NSIS_URL_INFO_ABOUT "${CPACK_PACKAGE_HOMEPAGE_URL}")
        set(CPACK_NSIS_MODIFY_PATH ON)
        set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)

        list(APPEND CPACK_GENERATOR NSIS)
    endif()

    # NuGet
    # TODO: Find a more robust means of detecting whether NuGet is available
    find_program(NUGET_EXECUTABLE nuget)
    if(NUGET_EXECUTABLE)
        message(STATUS "NuGet found at ${NUGET_EXECUTABLE}; needs configuration")

        # list(APPEND CPACK_GENERATOR NuGet)
    endif()

    # Set up WIX
    # TODO: Make this user-configurable or otherwise deal with version and architecture in WIX path
    set(WIX_ROOT_DIR "/Program Files (x86)/WiX Toolset v3.11")
    find_package(WIX)
    # message("WIX found? ${WIX_FOUND}")
    if(WIX_FOUND)
        # The variable holding the WIX root directory used by CPack is different from the
        # variable populated by find_package(WIX) i.e. cmake/FindWix.cmake
        # Manually tell CPack where find_package() found WIX... 
        set(CPACK_WIX_ROOT "${WIX_ROOT_DIR}")
        # Manually convert LICENSE to RTF format because WIX/CPack is stupid
        set(WIX_LICENSE_FILE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.rtf")
        set(CPACK_WIX_LICENSE_RTF "${WIX_LICENSE_FILE}")
        install(FILES "${WIX_LICENSE_FILE}" DESTINATION doc)
        set(CPACK_WIX_PRODUCT_ICON "${CMAKE_CURRENT_SOURCE_DIR}/img/SOFIRE2.ico")
        # Note: This should not change for the life of the product
        # Generated from guidgen.exe
        set(CPACK_WIX_UPGRADE_GUID "7FE5EEF5-79E7-428D-BEBB-DDDF6ED80747")

        list(APPEND CPACK_GENERATOR WIX)
    endif()
else()
    list(APPEND CPACK_GENERATOR TGZ TBZ2)
    if(APPLE)
        # Set up DRAGNDROP
        # Add DragNDrop properties
        set(CPACK_DMG_VOLUME_NAME "${CPACK_PACKAGE_NAME} v${CPACK_PACKAGE_VERSION}")
#        set(CPACK_DMG_FORMAT "UDZO")
        set(CPACK_DMG_FORMAT "UDBZ")
#*       CPACK_DMG_DS_STORE
#*       CPACK_DMG_DS_STORE_SETUP_SCRIPT
#*       CPACK_DMG_BACKGROUND_IMAGE
#        CPACK_DMG_DISABLE_APPLICATIONS_SYMLINK
#        CPACK_DMG_SLA_DIR
#        CPACK_DMG_SLA_LANGUAGES
#        CPACK_DMG_<component>_FILE_NAME
#        CPACK_COMMAND_HDIUTIL
#        CPACK_COMMAND_SETFILE
#        CPACK_COMMAND_REZ
        list(APPEND CPACK_GENERATOR DragNDrop)
    else()
        # Set up DEB
        # TODO: Find a more robust means of detecting whether debian packaging should be enabled
        # Note that readelf is not strictly necessary but platform is assumed
        # Debian-ish if it's present 
        find_program(READELF_EXECUTABLE readelf)
        if(READELF_EXECUTABLE)
            set(CPACK_DEBIAN_PACKAGE_DESCRIPTION
"${CPACK_PACKAGE_DESCRIPTION_SUMMARY}
SOFIRE II solves a set of equations describing the heat and mass transfer by the finite
difference method to simulate a sodium pool fire in a single containment volume or in
an interconnected double cell to predict the consequences of sodium pool fires within
a facility.
")
            # set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "any")
            # Auto-detect dependencies
            set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
            # Hack to find internal libraries - see https://gitlab.kitware.com/cmake/cmake/-/issues/17447
            # list(APPEND CMAKE_INSTALL_RPATH
            #     "$ORIGIN/../lib/graphviz"
            # )
            # A better solution is to set LD_LIBRARY_PATH when running CPack or `make package`
            # Exanple: LD_LIBRARY_PATH=./_CPack_Packages/Linux/DEB/Graphviz-2.45.0-Linux/usr/lib/graphviz:${LD_LIBRARY_PATH} make package
            # Exanple: LD_LIBRARY_PATH=./_CPack_Packages/Linux/DEB/Graphviz-2.45.0-Linux/usr/lib/graphviz:${LD_LIBRARY_PATH} cpack -G DEB
            # Build multiple packages
            set(CPACK_DEB_COMPONENT_INSTALL ON)
            set(CPACK_DEBIAN_ENABLE_COMPONENT_DEPENDS ON)
            # set(CPACK_DEBIAN_PACKAGE_DEBUG ON)
            # Turn off for executable-only; only needed for packaging libraries
            set(CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS OFF)

            list(APPEND CPACK_GENERATOR DEB)
        endif()

        # Set up RPM
        # TODO: Find a more robust means of detecting whether RPM generator is available
        find_program(RPMBUILD_EXECUTABLE rpmbuild)
        if(RPMBUILD_EXECUTABLE)
            message("rpmbuild found at ${RPMBUILD_EXECUTABLE}; needs configuration")
            
            # Needs additional work
            # list(APPEND CPACK_GENERATOR RPM)
        endif()
    endif()
endif()

# This must be last
include(CPack)
