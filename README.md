# SOFIRE2: A Simple Sodium Pool Fire Model for Liquid Metal Cooled Reactor Containment Analysis

## Overview

This package is based on the source code published in AEC Research and
Development Report AI-AEC-1305 *SOFIRE II User Report*, issued
March 30, 1973. See [https://doi.org/10.2172/4490831]

The original code was written for FORTRAN H under (IBM) OS/360; see
[http://www.bitsavers.org/pdf/ibm/360/fortran/Y28-6642-3_FortH_PLM_Nov68.pdf]

The initial goals of this project are:

* (**DONE**, one-cell code) recover the original source code as published in AI-AEC-1305 to a compilable
  runnable state on x86_64 hardware
* (**DONE**, one-cell code) update the source code to well-formed Fortran 2018,
* benchmark the revised code
  * against experimental data (**in progress**, one-cell code - issues reconstructing benchmark input), and
  * (**DONE**, one-cell code) against previous code revisions,
* (**DONE**, one-cell code) reconstitute a sustainable cross-platform build/test/packaging process
  based on CMake/CTest/CPack,
* (**DONE**, one-cell code) address numerical problems in the original code (i.e. loss of precision
  leading to "clipping"; see page C-13 of AI-AEC-1305). See also ANL-ARC-250
  "Sodium pool fire phenomena, sodium pool fire modeling in SOFIRE II,
  and SOFIRE II calculations for the AFR-100", [https://doi.org/10.2172/1054875]

## Current Status

### One-Cell Code: sofire2_1c

* Code compiles and produces results roughly similar to the reference data.
  Comparison between original 1973 single precision code and modernized code
  (both single and double precision) is documented in the developers guide.
* Errors have been corrected
  * Inadvertent use of `ZF2` in calculation  of `CAPF3` and `CAPF4` has been fixed;
    see Section 5, paragraph 2 on page 17 of ANL-ARC-250 for details. This error did
    not affect transient output in sample case (transient output was
    numerically identical before and after correction).
* One-cell code has been extensively refactored and modernized:
  * Code has been upgraded to `IMPLICIT NONE` with `intent` set on all subprogram
    arguments
  * Most constants have been extracted to modules and documented
  * Floating-point precision has been parameterized and code now uses double
    precision uniformly
  * Static arrays have been converted to dynamically-allocated list,
    allowing code to run to completion; original code stopped short due to
    overwriting end of fixed output arrays
  * Better file diagnostics allowing for graceful termination of code if input
    errors occur
  * All COMMON blocks and intermediate input reading routines have been
    refactored away
  * All subordinate routines moved to modules
* CMake generates test results and properly compares output files with references
* CPack correctly generates many archives and installation packages
  * Static archives on Windows and Linux
  * NSIS (`.exe`) and WIX (`.msi`) installers on Windows
  * DEB (`.deb`) installer on Linux
  * DragNDrop (`.dmg`) installer on OSX
  * RPM (`.rpm`) installer on Linux has not yet been configured or tested
* Doxygen generates PDF developers guide from source code on Windows and Linux
  * Difficulties generating PDF under CI (LaTeX dependencies, overhead of texlive install)
* Code has been modified to produce CSV-formatted output to simplify plotting
  and comparison
* "clipping" behavior in one-cell code has been analyzed and documented (floor temperature
  of top layer of concrete - node 2). Minor clipping occurs in single precision modernized
  code but double precision modernized code produces smoother, more physically reasonable
  results consistent with ANL-ARC-250 findings.
* Benchmark cases have been developed along with Gnuplot plot files. Benchmarks run but are
  inaccurate and it is believed the SOFIRE2 input files are incomplete. This issue requires
  further research.

### Two-Cell Code: sofire2_2c

The two-cell code has not yet been addressed.

### Documentation

* Parts I-IV (Introduction, Theory, Code Operation, Experimental Verification of Model),
  Appendices B, C, E, and F and 'Nomenclature and Definitions' are believed complete.
  Verification section only contains original 1973 verification data however a new code
  comparison section has been created which is consistent with the results shown in ANL-ARC-250
* Documentation for the input data instructions (Appendices B and E) have been migrated
  to the current report
* Documentation for the transient output quantities (Appendices C and F) has been
  migrated to the current report
* Source code appendices A and D have been omitted. Complete source is available for A,
  not yet for D.
* Sample tabular output is omitted; elements will be moved to a separate 'Testing' section.

## Dependencies

### Required Tools

SOFIRE2 itself has no external dependencies however the combined build,
documentation, test, and packaging system requires CMake and a recent Fortran
compiler (supports at least F2008), for example `gfortran` via the GCC collection.
On Windows, `gfortran` and the `ninja` build system are known to work and on
Linux `gfortran` should work either with `make` or `ninja`. OSX builds work with
`gfortran` and `make` installed via Fink.

### Optional Tools

Optional developer documentation is produced using Doxygen which expects to find
`dot` from Graphviz. PDF documentation is generated via `LaTeX`; the TeXLive
distribution is known to work on both Windows and Linux and additionally Perl
is necessary to support bibliography and index processing. The code comparison
documentation relies on plots generated via Gnuplot.

Packaging utilities vary by operating system but on Windows both WIX and NSIS
are supported. ZIP archives are supported by default on all platforms,
`.tar.gz` and `.tar.bz2` archives are supported by default on unix-like systems
(OSX, Linux). DragNDrop (`.dmg`) installers are created on OSX systems and
`.deb` packages are built on Debian-flavored Linux systems. CPack will only
attempt to build packages if the requisite packaging utilities are detected;
at a minimum, static zip archives are supported on every platform.

## Build Process

The software build process for SOFIRE2 follows the basic CMake build process:

* Download and unpack / pull source distribution 
* `cd` *project root*
* `mkdir build`
* `cd build`
* `cmake ..` with options like `-C Release` or `Debug`, `-G` *generator*, `-D` *config vars*
* build with `make`, `ninja`, *etc.* (depends on platform; see **Required Tools**). Using `make`:
  * `make`
  * `make test`
  * `make docs` *optional*
  * `make package` *optional*

Packaging with `make package` is strongly encouraged to allow easy distribution, installation, and removal

## License

The original code was produced by the Atomics International Division of
Rockwell International for the US Atomic Energy Commission. No license was
provided for the original 1973 code as was the practice at the time. For
the purpose of furthering education and research and to preserve the legacy
of this software, this project is released under the MIT License
(see LICENSE). It is not clear what the license should be but it is felt
that the MIT License is appropriate here.

## Image Credits

Uses icons from "Project Icons" by Mihaiciuc Bogdan. [http://bogo-d.deviantart.com]
Icon set license: CC Attribution 4.0
[https://iconarchive.com/icons/bogo-d/project/License.pdf]
