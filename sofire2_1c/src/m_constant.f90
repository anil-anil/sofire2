!> @file m_constant.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical and physical constants used by SOFIRE2
!!
!! @note All members are considered public
!!
!! @todo Provide CODATA or other references for physical constants
module m_constant
    use sofire2_param, only: WP
    implicit none

    ! *** Numerical constants ***

    !> Numerical constant - zero
    real(kind=WP), parameter :: ZERO = 0.0_WP

    !> Numerical constant - one
    real(kind=WP), parameter :: ONE = 1.0_WP

    !> Numerical constant - two
    real(kind=WP), parameter :: TWO = 2.0_WP

    !> Numerical constant - three
    real(kind=WP), parameter :: THREE = 3.0_WP

    !> Numerical constant - four
    real(kind=WP), parameter :: FOUR = 4.0_WP

    !> Numerical constant - ten
    real(kind=WP), parameter :: TEN = 10.0_WP

    !> Numerical constant - tenth \f$\left(\frac{1}{10}\right)\f$
    real(kind=WP), parameter :: TENTH = ONE / TEN

    !> Numerical constant - third \f$\left(\frac{1}{3}\right)\f$
    real(kind=WP), parameter :: THIRD = ONE / THREE

    !> Numerical constant - half \f$\left(\frac{1}{2}\right)\f$
    real(kind=WP), parameter :: HALF = ONE / TWO

    ! *** Physical constants ***

    !> Gravitational constant, \f$\US{32.2}{\foot\per\square\second}\f$
    real(kind=WP), parameter :: GIN = 32.2_WP

    !> Specific gas constant for air, \f$\US{53.3}{\foot\lbf\per\rankine\per\lbm}\f$ of air
    !! \todo Revise to referenced value
    real(kind=WP), parameter :: RIN = 53.3_WP

    !> Stefan-Boltzmann constant, \f$\US{1.714E-09}{\BTU\per\hour\per\square\foot\per\rankine^{4}}\f$
    !! \todo Revise to referenced value
    real(kind=WP), parameter :: SIGMA_SB = 1.714E-09_WP

    ! *** Unit conversion factors ***

    !> Zero Fahrenheit, \f$\US{460}{\rankine}\f$
    !! \todo Revise to \f$\US{459.67}{\rankine}\f$
    real(kind=WP), parameter :: T0R = 460.0_WP

    !> Seconds per minute, \f$\SI{60}{\second\per\minute}\f$
    real(kind=WP), parameter :: SEC_MIN = 60.0_WP

    !> Minutes per hour, \f$\SI{60}{\minute\per\hour}\f$
    real(kind=WP), parameter :: MIN_HR = 60.0_WP

    !> Seconds per hour, \f$\SI{3600}{\second\per\hour}\f$
    real(kind=WP), parameter :: SEC_HR = SEC_MIN * MIN_HR

    !> Inches per foot, \f$\US{12}{\inch\per\foot}\f$
    real(kind=WP), parameter :: IN_FT = 12.0_WP

! contains
end module m_constant
