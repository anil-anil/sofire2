!> @file sofire2_1c.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Main routine of SOFIRE II one-cell sodium pool fire simulator

!     SOFIRE II ENGINEERING CODE-SODIUMPOOL FIRE CLOSED CELL
!     REVISED 8-3-72

!> @brief Revised SOFIRE II one-cell sodium pool fire simulator
!! based on the 8/3/1972 source code published in
!! @cite Beiringer1973
!!
!! @todo Migrate constants to module parameters - partially implemented
!! @todo Verify and reference physical correlations
!! @todo Migrate intermediate variables to a `rates` object or to the results dataframe
!! @todo Refactor into main driver (handling UI and I/O), a setup routine, and a single-timestep calculation routine
program SOFIRE2_1C
    use sofire2_param, only: WP, NDP, ND
    use iso_fortran_env, only: stdin => input_unit,                     &
        stdout => output_unit, stderr => error_unit
    ! ! Static allocation
    ! use m_dataframe, only: result_dataframe, scenario_dataframe
    ! Dynamic allocation
    use m_dataframe, only: result_dlnode, scenario_dataframe
    use m_input, only: read_labels, read_values
    use m_output, only: get_csv_unit, close_csv_unit
    use m_constant, only: ZERO, TENTH, HALF, ONE, TWO, THREE, GIN, RIN, &
        SIGMA_SB, T0R, SEC_MIN, SEC_HR, IN_FT

    implicit none

    real(kind=WP) :: A
    real(kind=WP) :: AK1
    real(kind=WP) :: AK2
    real(kind=WP) :: B
    real(kind=WP) :: B1
    real(kind=WP) :: B2
! CAPC1  THERMAL CAPACITY 1ST CELL WALL NODE BTU/DEG F
    real(kind=WP) :: CAPC1
! CAPC2  THERMAL CAPACITY 2ND CELL WALL NODE BTU/DEG F
    real(kind=WP) :: CAPC2
! CAPC3  THERMAL CAPACITY 3RD CELL WALL NODE BTU/DEG F
    real(kind=WP) :: CAPC3
! CAPC4  THERMAL CAPACITY 4TH CELL WALL NODE BTU/DEG F
    real(kind=WP) :: CAPC4
! CAPF1  THERMAL CAPACITY 1ST CELL FLOOR NODE BTU/DEG F
    real(kind=WP) :: CAPF1
! CAPF2  THERMAL CAPACITY 2ND CELL FLOOR NODE BTU/DEG F
    real(kind=WP) :: CAPF2
! CAPF3  THERMAL CAPACITY 3RD CELL FLOOR NODE BTU/DEG F
    real(kind=WP) :: CAPF3
! CAPF4  THERMAL CAPACITY 4TH CELL FLOOR NODE BTU/DEG F
    real(kind=WP) :: CAPF4
    real(kind=WP) :: CAPS
! CAPS1  THERMAL CAPACITY 1ST NA NODE BTU/DEG F
    real(kind=WP) :: CAPS1
! CAPS2  THERMAL CAPACITY 2ND NA NODE BTU/DEG F
    real(kind=WP) :: CAPS2
! CAPS3  THERMAL CAPACITY 3RD NA NODE BTU/DEG F
    real(kind=WP) :: CAPS3
! CAPS4  THERMAL CAPACITY 4TH NA NODE BTU/DEG F
    real(kind=WP) :: CAPS4
    real(kind=WP) :: D
    real(kind=WP) :: D1
    real(kind=WP) :: D2
    real(kind=WP) :: DELC1
    real(kind=WP) :: DELC2
    real(kind=WP) :: DELC3
    real(kind=WP) :: DELC4
    real(kind=WP) :: DELF1
    real(kind=WP) :: DELF2
    real(kind=WP) :: DELF3
    real(kind=WP) :: DELF4
    real(kind=WP) :: DELGC
    real(kind=WP) :: DELS
    real(kind=WP) :: DELS1
    real(kind=WP) :: DELS2
    real(kind=WP) :: DELS3
    real(kind=WP) :: DELS4
    real(kind=WP) :: DIFF
    real(kind=WP) :: DTF1
    real(kind=WP) :: DTF2
    real(kind=WP) :: DTF3
    real(kind=WP) :: DTF4
    real(kind=WP) :: DTGASC
    real(kind=WP) :: DTS
    real(kind=WP) :: DTS1
    real(kind=WP) :: DTS2
    real(kind=WP) :: DTS3
    real(kind=WP) :: DTS4
    real(kind=WP) :: DTWC1
    real(kind=WP) :: DTWC2
    real(kind=WP) :: DTWC3
    real(kind=WP) :: DTWC4
    real(kind=WP) :: E
    real(kind=WP) :: EX1
    real(kind=WP) :: EX2
    real(kind=WP) :: EXX
    real(kind=WP) :: HF
    integer :: I
    integer :: II
    integer :: IX1
    integer :: IX2
    integer :: IX3
    real(kind=WP) :: OXLB
    real(kind=WP) :: PATM
    real(kind=WP) :: PIN
    real(kind=WP) :: T1
    real(kind=WP) :: T2
! TWC1I  TEMP CELL WALL NODE 1 DEG R
    real(kind=WP) :: TWC1I
! TWC2I  TEMP CELL WALL NODE 2 DEG R
    real(kind=WP) :: TWC2I
! TWC3I  TEMP CELL WALL NODE 3 DEG R
    real(kind=WP) :: TWC3I
! TWC4I  TEMP CELL WALL NODE 4 DEG R
    real(kind=WP) :: TWC4I
    real(kind=WP) :: XX1
    real(kind=WP) :: XX2
! Y1     ADMITTANCE NA TO FLOOR  BTU/HRDEG F
    real(kind=WP) :: Y1
! Y10    ADMITTANCE NA SURFACE TO 1ST NA NODE BTU/HRDEG F
    real(kind=WP) :: Y10
! Y11    ADMITTANCE TS1 TO TS2 BTU/HRDEG F
    real(kind=WP) :: Y11
! Y12    ADMITTANCE TS2 TO TS3 BTU/HRDEG F
    real(kind=WP) :: Y12
! Y13    ADMITTANCE TS3 TO TS4 BTU/HRDEG F
    real(kind=WP) :: Y13
! Y2     ADMITTANCE TFC1 TO TFC2 BTU/HRDEG F
    real(kind=WP) :: Y2
! Y3     ADMITTANCE TFC2 TO TFC3 BTU/HRDEG F
    real(kind=WP) :: Y3
! Y4     ADMITTANCE TFC3 TO TFC4 BTU/HRDEG F
    real(kind=WP) :: Y4
! Y5     ADMITTANCE TFC4 TO TA   BTU/HRDEG F
    real(kind=WP) :: Y5
! Y6     ADMITTANCE TWC4 TO TWC2 BTU/HRDEG F
    real(kind=WP) :: Y6
! Y7     ADMITTANCE TWC2 TO TWC3 BTU/HRDEG F
    real(kind=WP) :: Y7
! Y8     ADMITTANCE TWC3 TO TWC4 BTU/HRDEG F
    real(kind=WP) :: Y8
! Y9     ADMITTANCE TWC4 TO TA   BTU/HRDEG F
    real(kind=WP) :: Y9
    real(kind=WP) :: YCONV1
    real(kind=WP) :: YCONV2
    real(kind=WP) :: YRAD1
    real(kind=WP) :: YRAD2
    real(kind=WP) :: YRAD3
    real(kind=WP) :: YROD
    real(kind=WP) :: ZNAS
    real(kind=WP) :: ZNAS1
    real(kind=WP) :: ZNAS2
    real(kind=WP) :: ZNAS3
    real(kind=WP) :: ZNAS4

    ! Moved to result_dataframe class
    ! common /SAVE/ STGASC, STWC1, SXM, SPGASC, STF1, STF2, STWC2, STS1,  &
    !     ST

    ! Moved to result_dataframe class
    ! common /COMA/ TS, TF1, TF2, TF3, TF4, TGASC, TWC1, TWC2, TWC3,      &
    !     TWC4, T, XM, PGASC, OXR, TS1, TS2, TS3, TS4

    ! common // TSI,TS1I,TS2I,TS3I  ,TS4I  , TF1I ,TF2I ,TF3I ,TF4I,TWC1I &
    !     ,TWC2I,TWC3I ,TWC4I,TGASCI,PGASCI, TI   ,TA   ,PA   ,XMI ,XMAX  &
    !     ,AF   ,A1    ,A2   ,A5    ,CO    , ANA  ,QC   ,VOLAC,AF2 ,AF3   &
    !     ,SOD  ,PRT1  ,PRT2 ,PRT3  ,F3    , CON  ,X    ,S1   ,S2  ,S3    &
    !     ,S4   ,ZNAS  ,ZNAS1,ZNAS2 ,ZNAS3 , ZNAS4,ZF1  ,ZF2  ,ZF3 ,ZF4   &
    !     ,XWC1 ,XWC2  ,XWC3 ,XWC4  ,CPAC  , CPS  ,CPF1 ,CPF2 ,CPF3,CPF4  &
    !     ,CPWC1,CPWC2 ,CPWC3,CPWC4 ,AKS   , AKF1 ,AKF12,AKF2 ,AKF3,AKF4  &
    !     ,AKWC1,AKWC12,AKWC2,AKWC3 ,AKWC4 , RHOA ,RHS  ,RHF1, RHF2,RHF3  &
    !     ,RHF4 ,RHWC1 ,RHWC2,RHWC3 ,RHWC4 , ZF12 ,XWC12

    ! ! Static allocation - no linked-list history
    ! type(result_dataframe) :: result_prev
    ! type(result_dataframe) :: result_curr
    ! Dynamic allocation - history retained as doubly-linked-list
    type(result_dlnode), target :: result_head
    type(result_dlnode), pointer :: result_prev => null()
    type(result_dlnode), pointer :: result_curr => result_head

    type(scenario_dataframe) :: scenario

    ! Temporary input variables to work around COMMON block memory/access games
    ! logical :: use_common
    character(len=72) :: TITLE
    character(len=8), dimension(:), allocatable :: LABEL
    real(kind=WP), dimension(:), allocatable :: SAVINS
    real(kind=WP), dimension(:), allocatable :: PUTINS
    integer :: imemstatus

    integer :: inunit

    integer :: icase
    character(len=80) :: csvfn
    integer :: csvunit

! Formats

33  format('1')
50  format('0', 24X, 'TIME=', ES12.4, '     XM =', ES12.4, //,          &
        '  QCONV1=', ES12.5, '  QCONV2=', ES12.5, '     QROD=', ES12.5, &
        '   QRAD1=', ES12.5, '   QRAD2=', ES12.5, /,                    &
        '   QRAD3=', ES12.5, '   YRAD1=', ES12.5, '   YRAD2=', ES12.5,  &
        '   YRAD3=', ES12.5, '  YCONV1=', ES12.5, /,                    &
        '  YCONV2= ', ES12.5, '    YROD=', ES12.5, '   DELF1=', ES12.5, &
        '   DELF2=', ES12.5, '   DELF3=', ES12.5, /,                    &
        '   DELF4=', ES12.5, '   DELS1=', ES12.5, '    DELS=', ES12.5,  &
        '   DELGC=', ES12.5, '   DELC1=', ES12.5, /,                    &
        '   DELC2=', ES12.5, '   DELC3=', ES12.5, '   DELC4=', ES12.5,  &
        '   DELS2=', ES12.5, '   DELS3=', ES12.5, /,                    &
        '   DELS4=', ES12.5, /)
51  format('0', '      I=', I4, ' DTMIN1=', ES12.4, ' DTMIN2=', ES12.4, &
        '  DTMIN=', ES12.4, '      W=', ES12.4, /,                      &
        '   RHOC=', ES12.4, '  PGASC=', ES12.4, '     T1=', ES12.4,     &
        '     T2=', ES12.4, '     B1=', ES12.4, /,                      &
        '     B2=', ES12.4, '     D1=', ES12.4, '     D2=', ES12.4,     &
        '    GIN=', ES12.4, '    EXX=', ES12.4, /,                      &
        ' TGASC=', ES12.4, ' W1=', ES12.4, ' W2=', ES12.4,              &
        ' TS=', ES12.4, /)
500 format('ERROR: Cannot allocate memory for ', A, ' - exiting.')

    continue

    ! Local variables
    A = ZERO
    AK1 = ZERO
    AK2 = ZERO
    B = ZERO
    B1 = ZERO
    B2 = ZERO
! CAPC1  THERMAL CAPACITY 1ST CELL WALL NODE BTU/DEG F
    CAPC1 = ZERO
! CAPC2  THERMAL CAPACITY 2ND CELL WALL NODE BTU/DEG F
    CAPC2 = ZERO
! CAPC3  THERMAL CAPACITY 3RD CELL WALL NODE BTU/DEG F
    CAPC3 = ZERO
! CAPC4  THERMAL CAPACITY 4TH CELL WALL NODE BTU/DEG F
    CAPC4 = ZERO
! CAPF1  THERMAL CAPACITY 1ST CELL FLOOR NODE BTU/DEG F
    CAPF1 = ZERO
! CAPF2  THERMAL CAPACITY 2ND CELL FLOOR NODE BTU/DEG F
    CAPF2 = ZERO
! CAPF3  THERMAL CAPACITY 3RD CELL FLOOR NODE BTU/DEG F
    CAPF3 = ZERO
! CAPF4  THERMAL CAPACITY 4TH CELL FLOOR NODE BTU/DEG F
    CAPF4 = ZERO
    CAPS = ZERO
! CAPS1  THERMAL CAPACITY 1ST NA NODE BTU/DEG F
    CAPS1 = ZERO
! CAPS2  THERMAL CAPACITY 2ND NA NODE BTU/DEG F
    CAPS2 = ZERO
! CAPS3  THERMAL CAPACITY 3RD NA NODE BTU/DEG F
    CAPS3 = ZERO
! CAPS4  THERMAL CAPACITY 4TH NA NODE BTU/DEG F
    CAPS4 = ZERO
    D = ZERO
    D1 = ZERO
    D2 = ZERO
    DELC1 = ZERO
    DELC2 = ZERO
    DELC3 = ZERO
    DELC4 = ZERO
    DELF1 = ZERO
    DELF2 = ZERO
    DELF3 = ZERO
    DELF4 = ZERO
    DELGC = ZERO
    DELS = ZERO
    DELS1 = ZERO
    DELS2 = ZERO
    DELS3 = ZERO
    DELS4 = ZERO
    DIFF = ZERO
    DTF1 = ZERO
    DTF2 = ZERO
    DTF3 = ZERO
    DTF4 = ZERO
    DTGASC = ZERO
    DTS = ZERO
    DTS1 = ZERO
    DTS2 = ZERO
    DTS3 = ZERO
    DTS4 = ZERO
    DTWC1 = ZERO
    DTWC2 = ZERO
    DTWC3 = ZERO
    DTWC4 = ZERO
    E = ZERO
    EX1 = ZERO
    EX2 = ZERO
    EXX = ZERO
    HF = ZERO
    I = 0
    II = 0
    IX1 = 0
    IX2 = 0
    IX3 = 0
    OXLB = ZERO
    PATM = ZERO
    PIN = ZERO
    T1 = ZERO
    T2 = ZERO
    TWC1I = ZERO
    TWC2I = ZERO
    TWC3I = ZERO
    TWC4I = ZERO
    XX1 = ZERO
    XX2 = ZERO
! Y1     ADMITTANCE NA TO FLOOR  BTU/HRDEG F
    Y1 = ZERO
! Y10    ADMITTANCE NA SURFACE TO 1ST NA NODE BTU/HRDEG F
    Y10 = ZERO
! Y11    ADMITTANCE TS1 TO TS2 BTU/HRDEG F
    Y11 = ZERO
! Y12    ADMITTANCE TS2 TO TS3 BTU/HRDEG F
    Y12 = ZERO
! Y13    ADMITTANCE TS3 TO TS4 BTU/HRDEG F
    Y13 = ZERO
! Y2     ADMITTANCE TFC1 TO TFC2 BTU/HRDEG F
    Y2 = ZERO
! Y3     ADMITTANCE TFC2 TO TFC3 BTU/HRDEG F
    Y3 = ZERO
! Y4     ADMITTANCE TFC3 TO TFC4 BTU/HRDEG F
    Y4 = ZERO
! Y5     ADMITTANCE TFC4 TO TA   BTU/HRDEG F
    Y5 = ZERO
! Y6     ADMITTANCE TWC4 TO TWC2 BTU/HRDEG F
    Y6 = ZERO
! Y7     ADMITTANCE TWC2 TO TWC3 BTU/HRDEG F
    Y7 = ZERO
! Y8     ADMITTANCE TWC3 TO TWC4 BTU/HRDEG F
    Y8 = ZERO
! Y9     ADMITTANCE TWC4 TO TA   BTU/HRDEG F
    Y9 = ZERO
    YCONV1 = ZERO
    YCONV2 = ZERO
    YRAD1 = ZERO
    YRAD2 = ZERO
    YRAD3 = ZERO
    YROD = ZERO
    ZNAS = ZERO
    ZNAS1 = ZERO
    ZNAS2 = ZERO
    ZNAS3 = ZERO
    ZNAS4 = ZERO

    ! Dynamically allocate LABEL array
    imemstatus = 0
    if (allocated(LABEL)) then
        deallocate(LABEL)
    end if
    allocate(LABEL(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'LABEL'
        goto 999
    endif

    ! Dynamically allocate SAVINS array
    imemstatus = 0
    if (allocated(SAVINS)) then
        deallocate(SAVINS)
    end if
    allocate(SAVINS(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'SAVINS'
        goto 999
    endif
    
    ! Dynamically allocate PUTINS array
    imemstatus = 0
    if (allocated(PUTINS)) then
        deallocate(PUTINS)
    end if
    allocate(PUTINS(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'PUTINS'
        goto 999
    endif

    !> @todo Set inunit from command line or programmatically
    inunit = stdin

    ! Read scenario initial condition labels
    SAVINS = ZERO
    LABEL = '        '
    call read_labels(inunit, ND, LABEL)

    call scenario%init()
    call result_curr%init()

    icase = 0

    ! Calculate the next scenario, if any
L1: do

        ! Read initial condition title and values of next scenario.
        ! If this fails on read error, etc., all input conditions will be
        ! set to zero including scenario end time (XMAX).
        ! Gracefully exit if XMAX is not greater than zero. 
        PUTINS = ZERO
        call read_values(inunit, ND, LABEL, PUTINS, SAVINS, TITLE)
        call scenario%set_from_array(PUTINS)
        scenario%TITLE = TITLE

        ! Exit gracefully if scenario end time is not positive
        if (scenario%XMAX <= ZERO) then
            exit L1
        end if

        write(unit=stdout, fmt=33)

        ! Set initial conditions
        I = 1

        result_curr%TS = scenario%TSI
        result_curr%TS1 = scenario%TS1I
        result_curr%TS2 = scenario%TS2I
        result_curr%TS3 = scenario%TS3I
        result_curr%TS4 = scenario%TS4I
        result_curr%TF1 = scenario%TF1I
        result_curr%TF2 = scenario%TF2I
        result_curr%TF3 = scenario%TF3I
        result_curr%TF4 = scenario%TF4I
        result_curr%TGASC = scenario%TGASCI
        result_curr%TWC1 = scenario%TWC1I
        result_curr%TWC2 = scenario%TWC2I
        result_curr%TWC3 = scenario%TWC3I
        result_curr%TWC4  = scenario%TWC4I
        result_curr%XM = scenario%XMI
        result_curr%T = scenario%TI
        result_curr%PGASC = scenario%PGASCI
        result_curr%SUM1 = ZERO ! Preinitialized
        result_curr%SUM2 = ZERO ! Preinitialized

        XX1 = TWO * scenario%AKS * scenario%A1
        XX2 = scenario%CPS * scenario%A1 * scenario%RHS
        Y1 = ONE / (scenario%ZNAS4 / (scenario%AKS * scenario%A5 * TWO) &
                    + scenario%ZF1 / (scenario%AKF1 * scenario%A5 * TWO))
        Y2 = ONE / (scenario%ZF1 / (scenario%AKF1 * scenario%A5 * TWO)  &
                    + scenario%ZF12 / (scenario%AKF12 * scenario%A5)    &
                    + scenario%ZF2 / (scenario%AKF2 * scenario%A5 * TWO))
        Y3  = ONE / (scenario%ZF2 / (scenario%AKF2 * scenario%A5 * TWO) &
                    + scenario%ZF3 / (scenario%AKF3 * scenario%A5 * TWO))
        Y4  = ONE / (scenario%ZF3 / (scenario%AKF3 * scenario%A5 * TWO) &
                    + scenario%ZF4 / (scenario%AKF4 * scenario%A5 * TWO))
        Y5 = scenario%AKF4 * scenario%A5 * TWO / scenario%ZF4
        Y6 = ONE / (scenario%XWC1 / (scenario%AKWC1 * scenario%A2 * TWO) &
                    + scenario%XWC12 / (scenario%AKWC12 * scenario%A2)  &
                    + scenario%XWC2 / (scenario%AKWC2 * scenario%A2 * TWO))
        Y7  = ONE / (scenario%XWC2 / (scenario%AKWC2 * scenario%A2 * TWO) &
                    + scenario%XWC3 / (scenario%AKWC3 * scenario%A2 * TWO))
        Y8  = ONE / (scenario%XWC3 / (scenario%AKWC3 * scenario%A2 * TWO) &
                    + scenario%XWC4 / (scenario%AKWC4 * scenario%A2 * TWO))
        Y9 = scenario%AKWC4 * scenario%A2 * TWO / scenario%XWC4
        Y10 = XX1 / (scenario%ZNAS + scenario%ZNAS1)
        Y11 = XX1 / (scenario%ZNAS1 + scenario%ZNAS2)
        Y12 = XX1 / (scenario%ZNAS2 + scenario%ZNAS3)
        Y13 = XX1 / (scenario%ZNAS3 + scenario%ZNAS4)

        CAPS = XX2 * scenario%ZNAS

        CAPS1 = XX2 * scenario%ZNAS1
        CAPS2 = XX2 * scenario%ZNAS2
        CAPS3 = XX2 * scenario%ZNAS3
        CAPS4 = XX2 * scenario%ZNAS4

        CAPF1 = scenario%CPF1 * scenario%A5 * scenario%ZF1 * scenario%RHF1
        CAPF2 = scenario%CPF2 * scenario%A5 * scenario%ZF2 * scenario%RHF2
        CAPF3 = scenario%CPF3 * scenario%A5 * scenario%ZF3 * scenario%RHF3
        CAPF4 = scenario%CPF4 * scenario%A5 * scenario%ZF4 * scenario%RHF4

        CAPC1 = scenario%CPWC1 * scenario%A2                            &
                * scenario%XWC1 * scenario%RHWC1
        CAPC2 = scenario%CPWC2 * scenario%A2                            &
                * scenario%XWC2 * scenario%RHWC2
        CAPC3 = scenario%CPWC3 * scenario%A2                            &
                * scenario%XWC3 * scenario%RHWC3
        CAPC4 = scenario%CPWC4 * scenario%A2                            &
                * scenario%XWC4 * scenario%RHWC4

        result_curr%RHOC = scenario%PGASCI / (RIN * scenario%TGASCI)
        
        result_curr%W = result_curr%RHOC * scenario%VOLAC

        PATM = scenario%PA * 0.1926_WP

        result_curr%DTMIN1 = ZERO ! Preinitialized
        result_curr%DTMIN2 = ZERO ! Preinitialized
        result_curr%W1 = ZERO ! Preinitialized
        result_curr%W2 = ZERO ! Preinitialized

        A = CAPS1
        B = CAPS2
        E = CAPS3
        D = CAPS4

        result_curr%C = scenario%CO

        IX1 = int(scenario%PRT1)
        IX2 = int(scenario%PRT2)
        IX3 = int(scenario%PRT3)

        T1 = HALF * (result_curr%TGASC + result_curr%TS)
        T2 = HALF * (result_curr%TGASC + result_curr%TWC1)

        B1 = ONE / T1
        B2 = ONE / T2

        D1 = ((4.94E-05_WP * T1 + 0.0188_WP)                            &
            / (result_curr%RHOC * 3600.0_WP))**2

        D2 = ((4.94E-05_WP * T2 + 0.0188_WP)                            &
            / (result_curr%RHOC * 3600.0_WP))**2

        AK1 = 0.014_WP + 1.92E-05_WP * (T1 - T0R)
        AK2 = 0.014_WP + 1.92E-05_WP * (T2 - T0R)

        EX1 = (GIN * B1 / D1                                            &
            * abs(result_curr%TS - result_curr%TGASC))**0.3333_WP
        EX2 = (GIN * B2 / D2                                            &
            * abs(result_curr%TGASC - result_curr%TWC1))**0.3333_WP

        result_curr%QCONV1 = 0.14_WP * scenario%A1 * AK1                &
            * (result_curr%TS - result_curr%TGASC) * EX1
        result_curr%QCONV2 = 0.27_WP * scenario%A2 * AK2                &
            * (result_curr%TGASC - result_curr%TWC1) * EX2

        result_curr%QROD = scenario%AF * SIGMA_SB                       &
            * (result_curr%TS**4 - result_curr%TWC1**4) * scenario%A1

        result_curr%QRAD1 = scenario%AF2 * SIGMA_SB                     &
            * (result_curr%TF1**4 - result_curr%TF2**4) * scenario%A5

        result_curr%QRAD2 = scenario%AF2 * SIGMA_SB                     &
            * (result_curr%TWC1**4 - result_curr%TWC2**4) * scenario%A2

        result_curr%QRAD3 = scenario%AF3 * SIGMA_SB                     &
            * (result_curr%TS**4 - result_curr%TGASC**4) * scenario%A1

        YRAD1 = result_curr%QRAD1 / (result_curr%TF1 - result_curr%TF2)

        YRAD2 = result_curr%QRAD2 / (result_curr%TWC1 - result_curr%TWC2)

        YRAD3 = result_curr%QRAD3 / (result_curr%TS - result_curr%TGASC)

        YCONV1 = result_curr%QCONV1 / (result_curr%TS - result_curr%TGASC)

        YCONV2 = result_curr%QCONV2 / (result_curr%TGASC - result_curr%TWC1)

        YROD = result_curr%QROD / (result_curr%TS - result_curr%TWC1)

        DELF1 = CAPF1 / (Y1 + Y2 + YRAD1)
        DELF2 = CAPF2 / (Y3 + Y2 + YRAD1)
        DELF3 = CAPF3 / (Y3 + Y4)
        DELF4 = CAPF4 / (Y4 + Y5)

        DELS1 = CAPS1 / (Y10 + Y11)
        DELS2 = CAPS2 / (Y11 + Y12)
        DELS3 = CAPS3 / (Y12 + Y13)
        DELS4 = CAPS4 / (Y13 + Y1)

        DELS = CAPS / (Y10 + YRAD3 + YCONV1 + YROD)

        DELGC = result_curr%RHOC * scenario%CPAC * scenario%VOLAC       &
            / (YCONV2 + YRAD3 + YCONV1)

        DELC1 = CAPC1 / (YRAD2 + Y6 + YCONV2 + YROD)
        DELC2 = CAPC2 / (YRAD2 + Y6 + Y7)
        DELC3 = CAPC3 / (Y7 + Y8)
        DELC4 = CAPC4 / (Y8 + Y9)

        result_curr%OX = ZERO ! Preinitialized
        result_curr%F40 = ZERO ! Preinitialized
        result_curr%DT = ZERO ! Preinitialized
        result_curr%DTMIN = ZERO ! Preinitialized

        ! Display initial rates
        write(unit=stdout, fmt=50)                                      &
            result_curr%T, result_curr%XM, result_curr%QCONV1,          &
            result_curr%QCONV2, result_curr%QROD, result_curr%QRAD1,    &
            result_curr%QRAD2, result_curr%QRAD3, YRAD1,                &
            YRAD2, YRAD3, YCONV1,                                       &
            YCONV2, YROD, DELF1,                                        &
            DELF2, DELF3, DELF4,                                        &
            DELS1, DELS, DELGC,                                         &
            DELC1, DELC2, DELC3,                                        &
            DELC4, DELS2, DELS3,                                        &
            DELS4

        ! Display T=0 results (initial quiescient conditions)

        call result_curr%write_legacy_output(stdout)

        if (icase <= 0) then
            ! Omit counter digit on output CSV file name for first case
            csvfn = "sofire2_results.csv"
        else
            ! Add counter digit to output CSV file name for subsequent cases
            write(csvfn, fmt='("sofire2_results_", I0, ".csv")') icase
        end if

        csvunit = get_csv_unit(csvfn)

        call result_curr%write_csv_header(csvunit)
        call result_curr%write_csv_data(csvunit)
    
        call result_curr%update_summary_info(scenario%PA)

L75:    do while (result_curr%T < scenario%XMAX)

            ! Set expected number of timesteps to iterate over BETWEEN
            ! PRODUCING OUTPUT
            if (result_curr%T > THREE) then
                ! Long term (T > 3hr) steps between output
                II = IX3
            else if (result_curr%T > ONE) then
                ! Intermediate term (1 hr < T <= 3hr) steps between output
                II = IX2
            else
                ! Short term (T <= 1hr) steps between output
                II = IX1
            end if

L1000:      do I = 2, II
                ! ! Static allocation maintaining no in-core history
                ! ! Declare result_prev and result_curr with
                ! ! type(result_dataframe) :: result_prev, result_curr
                ! result_prev = result_curr

                ! Append node to list, update prev and current pointers,
                ! initialize current value
                result_prev => result_curr
                imemstatus = 0
                allocate(result_curr%next, stat=imemstatus)
                if (imemstatus /= 0) then
                    write(unit=stderr, fmt=500) 'result_curr%next'
                    call close_csv_unit(csvunit)
                    exit L1
                end if
                result_curr => result_curr%next

                call result_curr%init()

                YCONV1 = result_prev%QCONV1                             &
                    / (result_prev%TS - result_prev%TGASC)
                YCONV2 = result_prev%QCONV2                             &
                    / (result_prev%TGASC - result_prev%TWC1)
                YRAD3 = result_prev%QRAD3                               &
                    / (result_prev%TS - result_prev%TGASC)
                YROD = result_prev%QROD                                 &
                    / (result_prev%TS - result_prev%TWC1)

                DELS1 = CAPS1 / (Y10 + Y11)
                DELS2 = CAPS2 / (Y11 + Y12)
                DELS3 = CAPS3 / (Y12 + Y13)
                DELS4 = CAPS4 / (Y13 + Y1)

                result_curr%DTMIN1 =                                    &
                    CAPS / (Y10 + YRAD3 + YCONV1 + YROD)
                result_curr%DTMIN2 = result_prev%RHOC * scenario%CPAC   &
                    * scenario%VOLAC / (YCONV2 + YRAD3 + YCONV1)
                result_curr%DT =                                        &
                    min(result_curr%DTMIN1, result_curr%DTMIN2,         &
                        DELF1, DELF2, DELF3, DELF4,                     &
                        DELS1, DELS2, DELS3,                            &
                        DELC1, DELC2, DELC3, DELC4)

                result_curr%DTMIN = result_curr%DT * scenario%X

                result_curr%T = result_prev%T + result_curr%DTMIN

                PIN = result_prev%PGASC * 0.19261_WP
                
                result_curr%W1 = scenario%F3 * result_prev%RHOC         &
                    * result_curr%DTMIN * SEC_MIN

                !> @todo Preserve original value of result_prev%TGASC
                !! and use a new variable to track adjusted gas temperature
                if (PATM < PIN) then
                    ! Outleakage
                    result_curr%F40 = scenario%CON * (PIN - PATM)**HALF
                    result_curr%W2 = result_curr%F40 * result_prev%RHOC &
                        * result_curr%DTMIN * SEC_MIN
                    result_curr%C =                                     &
                        (result_prev%W * result_prev%C                  &
                        - result_curr%W2 * result_prev%C                &
                        - result_curr%W1 * result_prev%C)               &
                        / (result_prev%W                                &
                           - result_curr%W2                             &
                           - result_curr%W1)
                    result_prev%TGASC =                                 &
                        (result_prev%W * result_prev%TGASC              &
                        - result_curr%W2 * result_prev%TGASC            &
                        - result_curr%W1 * result_prev%TGASC)           &
                        / (result_prev%W                                &
                           - result_curr%W1                             &
                           - result_curr%W2)
                    result_curr%W =                                     &
                        result_prev%W                                   &
                        - result_curr%W2                                &
                        - result_curr%W1
                else
                    ! Inleakage
                    result_curr%F40 = scenario%CON * (PATM - PIN)**HALF
                    result_curr%W2 = result_curr%F40 * scenario%RHOA    &
                        * result_curr%DTMIN * SEC_MIN
                    result_curr%C =                                     &
                        (result_prev%W * result_prev%C                  &
                        + result_curr%W2 * scenario%CO                  &
                        - result_curr%W1 * result_prev%C)               &
                        / (result_prev%W                                &
                           + result_curr%W2                             &
                           - result_curr%W1)
                    result_prev%TGASC =                                 &
                        (result_curr%W2 * scenario%TA                   &
                        - result_curr%W1 * result_prev%TGASC            &
                        + result_prev%W * result_prev%TGASC)            &
                        / (result_curr%W2                               &
                           - result_curr%W1                             &
                           + result_prev%W)
                    result_curr%W =                                     &
                        result_prev%W                                   &
                        + result_curr%W2                                &
                        - result_curr%W1
                end if
                result_curr%RHOC = result_curr%W / scenario%VOLAC

                result_prev%PGASC = result_curr%RHOC * RIN              &
                    * result_prev%TGASC

                result_curr%OX = scenario%VOLAC * result_curr%C         &
                    * result_curr%RHOC

                T1 = HALF * (result_prev%TGASC + result_prev%TS)
                T2 = HALF * (result_prev%TGASC + result_prev%TWC1)

                B1 = ONE / T1
                B2 = ONE / T2

                D1 = ((4.94E-05_WP * T1 + 0.0188_WP)                    &
                    / (result_curr%RHOC * 3600.0_WP))**2
                D2 = ((4.94E-05_WP * T2 + 0.0188_WP)                    &
                    / (result_curr%RHOC * 3600.0_WP))**2

                AK1 = 0.014_WP + 1.92E-05_WP * (T1 - T0R)
                AK2 = 0.014_WP + 1.92E-05_WP * (T2 - T0R)

                EXX = GIN * B1 / D1                                     &
                    * abs(result_prev%TS - result_prev%TGASC)

                if (EXX < ZERO) then
                    write(unit=stdout, fmt=51)                          &
                        I, result_curr%DTMIN1, result_curr%DTMIN2,      &
                        result_curr%DTMIN, result_curr%W,               &
                        result_curr%RHOC, result_prev%PGASC,            &
                        T1, T2, B1, B2, D1, D2, GIN, EXX,               &
                        result_prev%TGASC, result_curr%W1,              &
                        result_curr%W2, result_prev%TS
                    ! End current scenario; start next scenario (if any)
                    call close_csv_unit(csvunit)
                    cycle L1
                end if

                EX1 = EXX**0.3333_WP

                result_curr%QCONV1 = 0.14_WP * scenario%A1 * AK1        &
                    * (result_prev%TS - result_prev%TGASC) * EX1

                EX2 = (GIN * B2 / D2                                    &
                    * abs(result_prev%TGASC                             &
                          - result_prev%TWC1))**0.3333_WP

                result_curr%QCONV2 = 0.27_WP * scenario%A2 * AK2        &
                    * (result_prev%TGASC - result_prev%TWC1) * EX2

                result_curr%QROD = scenario%AF * SIGMA_SB               &
                    * (result_prev%TS**4 - result_prev%TWC1**4)         &
                    * scenario%A1

                result_curr%QRAD3 = scenario%AF3 * SIGMA_SB             &
                    * (result_prev%TS**4 - result_prev%TGASC**4)        &
                    * scenario%A1

                if ((scenario%SOD - result_prev%SUM2) > TENTH) then
                    DIFF = 241.57_WP / (132.0_WP + T1 / 1.8_WP)         &
                        * (T1 / 493.2_WP)**2.5_WP
                    HF = 0.075_WP * DIFF * EX1
                    result_curr%XM = scenario%ANA * HF                  &
                        * result_curr%RHOC * result_curr%C
                    OXLB = result_curr%XM * scenario%A1                 &
                        * result_curr%DTMIN / scenario%ANA
                    result_curr%SUM1 = OXLB + result_prev%SUM1
                    result_curr%SUM2 = result_curr%SUM1 * scenario%ANA
                end if

                if ((scenario%SOD - result_curr%SUM2) <= TENTH) then
                    result_curr%XM = ZERO
                end if

                DTGASC = result_curr%DTMIN                              &
                    / (result_curr%RHOC                                 &
                       * scenario%CPAC                                  &
                       * scenario%VOLAC)                                &
                    * (result_curr%QCONV1 + result_curr%QRAD3           &
                    - result_curr%QCONV2)
                result_curr%TGASC = result_prev%TGASC + DTGASC

                DTS = result_curr%DTMIN / CAPS                          &
                    * (result_curr%XM * scenario%A1 * scenario%QC       &
                    - result_curr%QCONV1                                &
                    - result_curr%QROD - result_curr%QRAD3              &
                    - Y10 * (result_prev%TS - result_prev%TS1))
                result_curr%TS = result_prev%TS + DTS

                CAPS1 = A + scenario%S1 * result_curr%T
                DTS1 = result_curr%DTMIN / CAPS1                        &
                * (Y10 * (result_prev%TS - result_prev%TS1)             &
                    - Y11 * (result_prev%TS1 - result_prev%TS2))
                result_curr%TS1 = result_prev%TS1 + DTS1

                CAPS2 = B + scenario%S2 * result_curr%T
                DTS2 = result_curr%DTMIN / CAPS2                        &
                    * (Y11 * (result_prev%TS1 - result_prev%TS2)        &
                    - Y12 * (result_prev%TS2 - result_prev%TS3))
                result_curr%TS2 = result_prev%TS2 + DTS2

                CAPS3 = E + scenario%S3 * result_curr%T
                DTS3 = result_curr%DTMIN / CAPS3                        &
                    * (Y12 * (result_prev%TS2 - result_prev%TS3)        &
                    - Y13 * (result_prev%TS3 - result_prev%TS4))
                result_curr%TS3 = result_prev%TS3 + DTS3

                CAPS4 = D + scenario%S4 * result_curr%T
                DTS4 = result_curr%DTMIN / CAPS4                        &
                    * (Y13 * (result_prev%TS3 - result_prev%TS4)        &
                    - Y1 * (result_prev%TS4 - result_prev%TF1))
                result_curr%TS4 = result_prev%TS4 + DTS4

                result_curr%QRAD1 = scenario%AF2 * SIGMA_SB             &
                    * (result_prev%TF1**4 - result_prev%TF2**4)         &
                    * scenario%A5
                DTF1 = result_curr%DTMIN / CAPF1                        &
                    * (Y1 * (result_prev%TS4 - result_prev%TF1)         &
                        - Y2 * (result_prev%TF1 - result_prev%TF2)      &
                        - result_curr%QRAD1)
                result_curr%TF1 = result_prev%TF1 + DTF1

                DTF2 = result_curr%DTMIN / CAPF2                        &
                    * (Y2 * (result_prev%TF1 - result_prev%TF2)         &
                    - Y3 * (result_prev%TF2 - result_prev%TF3)          &
                    + result_curr%QRAD1)
                result_curr%TF2 = result_prev%TF2 + DTF2

                DTF3 = result_curr%DTMIN / CAPF3                        &
                    * (Y3 * (result_prev%TF2 - result_prev%TF3)         &
                    - Y4 * (result_prev%TF3 - result_prev%TF4))
                result_curr%TF3 = result_prev%TF3 + DTF3

                DTF4 = result_curr%DTMIN / CAPF4                        &
                    * (Y4 * (result_prev%TF3 - result_prev%TF4)         &
                    - Y5 * (result_prev%TF4 - scenario%TA))
                result_curr%TF4 = result_prev%TF4 + DTF4

                result_curr%QRAD2 = scenario%AF2 * SIGMA_SB             &
                    * (result_prev%TWC1**4 - result_prev%TWC2**4)       &
                    * scenario%A2
                DTWC1 = result_curr%DTMIN / CAPC1                       &
                    * (result_curr%QCONV2                               &
                    - Y6 * (result_prev%TWC1 - result_prev%TWC2)        &
                    + result_curr%QROD - result_curr%QRAD2)
                result_curr%TWC1 = result_prev%TWC1 + DTWC1

                DTWC2 = result_curr%DTMIN / CAPC2                       &
                    * (Y6 * (result_prev%TWC1 - result_prev%TWC2)       &
                    - Y7 * (result_prev%TWC2 - result_prev%TWC3)        &
                    + result_curr%QRAD2)
                result_curr%TWC2 = result_prev%TWC2 + DTWC2

                DTWC3 = result_curr%DTMIN / CAPC3                       &
                    * (Y7 * (result_prev%TWC2 - result_prev%TWC3)       &
                    - Y8 * (result_prev%TWC3 - result_prev%TWC4))
                result_curr%TWC3 = result_prev%TWC3 + DTWC3

                DTWC4 = result_curr%DTMIN / CAPC4                       &
                    * (Y8 * (result_prev%TWC3 - result_prev%TWC4)       &
                    - Y9 * (result_prev%TWC4 - scenario%TA))
                result_curr%TWC4 = result_prev%TWC4 + DTWC4

                if (result_curr%XM /= ZERO) then
                    result_curr%RHOC = (result_curr%W - OXLB)           &
                        / scenario%VOLAC
                    result_curr%C = (result_curr%OX - OXLB)             &
                        / (result_curr%RHOC * scenario%VOLAC)
                    result_curr%W = result_curr%W - OXLB
                end if

                result_curr%OXR = result_curr%SUM2 * 0.2696_WP
                
                result_curr%PGASC = result_curr%RHOC * RIN              &
                    * result_curr%TGASC

            end do L1000

            call result_curr%update_summary_info(scenario%PA)

            call result_curr%write_legacy_output(stdout)

            call result_curr%write_csv_data(csvunit)

        end do L75

        call close_csv_unit(csvunit)

        icase = icase + 1

    end do L1

999 continue
    ! Clean up and exit

    ! Dynamic allocation only; remove next two lines if result_prev and
    ! result_prev are not pointers
    nullify(result_prev)
    nullify(result_curr)

    if (allocated(LABEL)) then
        deallocate(LABEL)
    end if
    if (allocated(SAVINS)) then
        deallocate(SAVINS)
    end if
    if (allocated(PUTINS)) then
        deallocate(PUTINS)
    end if

    ! Safe; does nothing if csvunit is stdout or is already closed
    call close_csv_unit(csvunit)

    stop
end program SOFIRE2_1C