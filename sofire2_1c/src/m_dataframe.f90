!> @file m_dataframe.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Data structures for tracking scenario (input) and transient
!! results (output) data
module m_dataframe
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    implicit none

    private

    !> Data structure containing all tracked transient results
    type, public :: result_dataframe
        ! Original tracked transient elements, COMMON /COMA/
        !> Scenario time, hours
        real(kind=WP) :: T = ZERO
        !> Mass of sodium oxide released to gas, \f$\us{\lbm}\f$
        real(kind=WP) :: OXR = ZERO
        !> Cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PGASC = ZERO
        !> Liner floor temperature, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF1 = ZERO
        !> Cell floor temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF2 = ZERO
        !> Cell floor temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF3 = ZERO
        !> Cell floor temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF4 = ZERO
        !> Cell gas temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TGASC = ZERO
        !> Sodium surface temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TS = ZERO
        !> Sodium pool temperature, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS1 = ZERO
        !> Sodium pool temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS2 = ZERO
        !> Sodium pool temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS3 = ZERO
        !> Sodium pool temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS4 = ZERO
        !> Liner wall temperature, center of first (inside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC1 = ZERO
        !> Cell wall temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC2 = ZERO
        !> Cell wall temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC3 = ZERO
        !> Cell wall temperature, center of fourth (outside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC4 = ZERO
        !> Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: XM = ZERO

        ! Previously untracked transient elements
        !> Convective heat transfer rate from sodium to gas, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QCONV1 = ZERO
        !> Convective heat transfer rate from gas to wall (liner?), \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QCONV2 = ZERO
        !> Radiative heat transfer rate from wall liner to wall node 2, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD1 = ZERO
        !> Radiative heat transfer rate from floor liner to floor node 2, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD2 = ZERO
        !> Radiative heat transfer rate from sodium pool to gas, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD3 = ZERO
        !> Radiative heat transfer rate from sodium pool to wall (liner?), \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QROD = ZERO
        !> Mass of oxygen remaining in cell, \f$\us{\lbm}\f$
        real(kind=WP) :: OX = ZERO
        !> Gas density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHOC = ZERO
        !> Mass of oxygen consumed, \f$\us{\lbm}\f$
        real(kind=WP) :: SUM1 = ZERO
        !> Mass of sodium burned, \f$\us{\lbm}\f$
        real(kind=WP) :: SUM2 = ZERO
        !> Mass of gas in cell, \f$\us{\lbm}\f$
        real(kind=WP) :: W = ZERO
        !> Volumetric gas leakage rate, \f$\us{\cubic\foot\per\hour}\f$
        real(kind=WP) :: F40 = ZERO
        !> Oxygen concentration corrected for leakage, weight-percent
        real(kind=WP) :: C = ZERO
        !> Gas leakage from cell due to exhaust, \f$\us{\lbm}\f$
        real(kind=WP) :: W1 = ZERO
        !> Gas inflow/outflow mass from cell due to pressure, \f$\us{\lbm}\f$
        real(kind=WP) :: W2 = ZERO
        !> Timestep based on gas mass, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN1 = ZERO
        !> Timestep based on sodium pool surface, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN2 = ZERO
        !> Minimum timestep, \f$\si{\hour}\f$
        real(kind=WP) :: DT = ZERO
        !> Stabilized timestep, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN = ZERO

        ! Summary results
        !> Summary value, scenario time, \f$\si{\hour}\f$
        real(kind=WP) :: ST = ZERO
        !> Summary value, cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ gauge
        real(kind=WP) :: SPGASC = ZERO
        !> Summary value, liner floor temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STF1 = ZERO
        !> Summary value, cell floor temperature, center of second node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STF2 = ZERO
        !> Summary value, cell gas temperature, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STGASC = ZERO
        !> Summary value, sodium pool temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STS1 = ZERO
        !> Summary value, liner wall temperature, center of first (inside) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STWC1 = ZERO
        !> Summary value, cell wall temperature, center of second node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STWC2 = ZERO
        !> Summary value, Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: SXM = ZERO

        contains
        procedure :: init => result_dataframe_init
        procedure :: write_legacy_output => result_dataframe_write_legacy_output
        procedure :: write_csv_header => result_dataframe_write_csv_header
        procedure :: write_csv_data => result_dataframe_write_csv_data
        procedure :: update_summary_info => result_dataframe_update_summary_info
        procedure :: write_summary_csv_header => result_dataframe_write_summary_csv_header
        procedure :: write_summary_csv_data => result_dataframe_write_summary_csv_data
    end type result_dataframe

    ! !> Single node of transient results (for singly-linked list) 
    ! type, public, extends(result_dataframe) :: result_slnode
    !     !> Link to next node in SLL
    !     type(result_slnode), pointer :: next => null()
    ! end type result_slnode

    !> Single node of transient results (for doubly-linked list) 
    type, public, extends(result_dataframe) :: result_dlnode
        !> Link to next node in SLL
        type(result_dlnode), pointer :: prev => null()
        type(result_dlnode), pointer :: next => null()
    end type result_dlnode
    
    !> Data structure containing all scenario initial conditions
    type, public :: scenario_dataframe
        !> Scenario title
        character(len=72) :: TITLE = ''

        !> Number of numerical parameters associated with scenario
        integer :: NPARAMS = 87

        !> Initial sodium surface temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TSI = ZERO
        !> Initial sodium temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TS1I = ZERO
        !> Initial sodium temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TS2I = ZERO
        !> Initial sodium temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TS3I = ZERO
        !> Initial sodium temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TS4I = ZERO
        !> Initial cell floor temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TF1I = ZERO
        !> Initial cell floor temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TF2I = ZERO
        !> Initial cell floor temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TF3I = ZERO
        !> Initial cell floor temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TF4I = ZERO
        !> Initial cell wall temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC1I = ZERO
        !> Initial cell wall temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC2I = ZERO
        !> Initial cell wall temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC3I = ZERO
        !> Initial cell wall temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC4I = ZERO
        !> Initial cell gas temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TGASCI = ZERO
        !> Initial cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PGASCI = ZERO
        !> Initial time, \f$\si{\hour}\f$
        real(kind=WP) :: TI = ZERO
        !> Ambient temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TA = ZERO
        !> Ambient pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PA = ZERO
        !> Initial sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: XMI = ZERO
        !> Problem end time, \f$\si{\hour}\f$
        real(kind=WP) :: XMAX = ZERO
        !> Emissivity * view factor, sodium pool surface to cell walls
        real(kind=WP) :: AF = ZERO
        !> Surface area of sodium pool, \f$\us{\square\foot}\f$
        real(kind=WP) :: A1 = ZERO
        !> Exposed cell wall surface area, \f$\us{\square\foot}\f$
        real(kind=WP) :: A2 = ZERO
        !> Wetted area of sodium pool, \f$\us{\square\foot}\f$
        real(kind=WP) :: A5 = ZERO
        !> Ambient oxygen concentration, volume-percent
        real(kind=WP) :: CO = ZERO
        !> Stoichiometric sodium combustion ratio \f$\us{\lbm\-\ce{Na}\per\lbm-\ce{O2}}\f$
        real(kind=WP) :: ANA = ZERO
        !> Heat of combustion, \f$\us{\BTU\per\lbm}\f$
        real(kind=WP) :: QC = ZERO
        !> Cell free volume, \f$\us{\cubic\foot}\f$
        real(kind=WP) :: VOLAC = ZERO
        !> Radiation exchange, floor liner to floor node 2, dimensionless
        real(kind=WP) :: AF2 = ZERO
        !> Radiation exchange, sodium pool to gas, dimensionless
        real(kind=WP) :: AF3 = ZERO
        !> Mass of sodium spilled, \f$\us{\lbm}\f$
        real(kind=WP) :: SOD = ZERO
        !> Print time 1, \f$\si{\hour}\f$
        real(kind=WP) :: PRT1 = ZERO
        !> Print time 2, \f$\si{\hour}\f$
        real(kind=WP) :: PRT2 = ZERO
        !> Print time 3, \f$\si{\hour}\f$
        real(kind=WP) :: PRT3 = ZERO
        !> Cell exhaust volumetric flow rate, \f$\us{\cubic\foot\per\hour}\f$
        real(kind=WP) :: F3 = ZERO
        !> Pressure leakage factor, TBD
        real(kind=WP) :: CON = ZERO
        !> Time step stabilizer, dimensionless
        real(kind=WP) :: X = ZERO
        !> Sodium removal rate, sodium node 1, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S1 = ZERO
        !> Sodium removal rate, sodium node 2, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S2 = ZERO
        !> Sodium removal rate, sodium node 3, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S3 = ZERO
        !> Sodium removal rate, sodium node 4, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S4 = ZERO
        !> Sodium surface layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS = ZERO
        !> Sodium node 1 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS1 = ZERO
        !> Sodium node 2 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS2 = ZERO
        !> Sodium node 3 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS3 = ZERO
        !> Sodium node 4 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS4 = ZERO
        !> Pan (floor node 1) thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF1 = ZERO
        !> Floor node 2 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF2 = ZERO
        !> Floor node 3 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF3 = ZERO
        !> Floor node 4 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF4 = ZERO
        !> Wall liner (node 1) thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC1 = ZERO
        !> Wall node 2 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC2 = ZERO
        !> Wall node 3 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC3 = ZERO
        !> Wall node 4 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC4 = ZERO
        !> Specific heat of air, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPAC = ZERO
        !> Specific heat of sodium, \f$\us{\BTU\per\lbm\per\fahrenheit}\f$
        real(kind=WP) :: CPS = ZERO
        !> Specific heat of burn pan (floor node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF1 = ZERO
        !> Specific heat of floor node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF2 = ZERO
        !> Specific heat of floor node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF3 = ZERO
        !> Specific heat of floor node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF4 = ZERO
        !> Specific heat of wall liner (node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC1 = ZERO
        !> Specific heat of wall node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC2 = ZERO
        !> Specific heat of wall node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC3 = ZERO
        !> Specific heat of wall node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC4 = ZERO
        !> Thermal conductivity of sodium, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKS = ZERO
        !> Thermal conductivity of pan (floor node 1), \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF1 = ZERO
        !> Thermal conductivity of air gap between floor nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF12 = ZERO
        !> Thermal conductivity of floor node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF2 = ZERO
        !> Thermal conductivity of floor node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF3 = ZERO
        !> Thermal conductivity of floor node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF4 = ZERO
        !> Thermal conductivity of wall node 1, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC1 = ZERO
        !> Thermal conductivity of air gap between wall nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC12 = ZERO
        !> Thermal conductivity of wall node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC2 = ZERO
        !> Thermal conductivity of wall node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC3 = ZERO
        !> Thermal conductivity of wall node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC4 = ZERO
        !> Cell gas density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHOA = ZERO
        !> Sodium density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHS = ZERO
        !> Burn pan (floor node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF1 = ZERO
        !> Floor node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF2 = ZERO
        !> Floor node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF3 = ZERO
        !> Floor node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF4 = ZERO
        !> Wall liner (node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC1 = ZERO
        !> Wall node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC2 = ZERO
        !> Wall node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC3 = ZERO
        !> Wall node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC4 = ZERO
        !> Gap width between burn pan (floor node 1) and floor node 2, \f$\us{foot}\f$
        real(kind=WP) :: ZF12 = ZERO
        !> Gap width between cell liner (wall node 1) and wall node 2, \f$\us{foot}\f$
        real(kind=WP) :: XWC12  = ZERO
    contains
        procedure :: init => scenario_dataframe_init
        procedure :: set_from_array => scenario_dataframe_set_from_array
    end type scenario_dataframe

contains

!> Initialize all result dataframe elements to zero
subroutine result_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(inout) :: this

    continue

    this%T = ZERO
    this%OXR = ZERO
    this%PGASC = ZERO
    this%TF1 = ZERO
    this%TF2 = ZERO
    this%TF3 = ZERO
    this%TF4 = ZERO
    this%TGASC = ZERO
    this%TS = ZERO
    this%TS1 = ZERO
    this%TS2 = ZERO
    this%TS3 = ZERO
    this%TS4 = ZERO
    this%TWC1 = ZERO
    this%TWC2 = ZERO
    this%TWC3 = ZERO
    this%TWC4 = ZERO
    this%XM = ZERO
    this%QCONV1 = ZERO
    this%QCONV2 = ZERO
    this%QRAD1 = ZERO
    this%QRAD2 = ZERO
    this%QRAD3 = ZERO
    this%QROD = ZERO
    this%OX = ZERO
    this%RHOC = ZERO
    this%SUM1 = ZERO
    this%SUM2 = ZERO
    this%W = ZERO
    this%F40 = ZERO
    this%C = ZERO
    this%W1 = ZERO
    this%W2 = ZERO
    this%DTMIN1 = ZERO
    this%DTMIN2 = ZERO
    this%DT = ZERO
    this%DTMIN = ZERO

    this%ST = ZERO
    this%SPGASC = ZERO
    this%STF1 = ZERO
    this%STF2 = ZERO
    this%STGASC = ZERO
    this%STS1 = ZERO
    this%STWC1 = ZERO
    this%STWC2 = ZERO
    this%SXM = ZERO

    return
end subroutine result_dataframe_init

!> Update transient results summary data
subroutine result_dataframe_update_summary_info(this, PA)
    use m_constant, only: T0R, IN_FT
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> Ambient pressure in lbf/ft**2
    real(kind=WP) :: PA

    continue

    this%STGASC = this%TGASC - T0R
    this%STWC1 = this%TWC1 - T0R
    this%SXM = this%XM
    this%SPGASC = (this%PGASC - PA) / IN_FT**2
    this%STF1 = this%TF1 - T0R
    this%STF2 = this%TF2 - T0R
    this%ST = this%T
    this%STWC2 = this%TWC2 - T0R
    this%STS1 = this%TS1 - T0R

    return
end subroutine result_dataframe_update_summary_info

!> Write transient results in the original SOFIRE II tabular output
!! format
subroutine result_dataframe_write_legacy_output(this, ounit)
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format('0', 24X, 'TIME =', ES12.4, '     XM =', ES12.4, //,         &
        '     TS =', ES12.5, '   TGASC=', ES12.5, '    TF1 =', ES12.5,  &
        '    TF2 =', ES12.5, '    TF3 =', ES12.5, /,                    &
        '    TF4 =', ES12.5, '   TWC1 =', ES12.5, '   TWC2 =', ES12.5,  &
        '   TWC3 =', ES12.5, '   TWC4 =', ES12.5, /,                    &
        '  QCONV1=', ES12.5, '  QCONV2=', ES12.5, ' PGASC =', ES12.5,   &
        '   QROD =', ES12.5, '  QRAD3 =', ES12.5, /,                    &
        '   TS1 =', ES12.5, '   SUM2 =', ES12.5, '    OX  =', ES12.5,   &
        '   RHOC =', ES12.5, '    SUM1=', ES12.5, /,                    &
        '     W  =', ES12.5, '    F40 =', ES12.5, '     C  =', ES12.5,  &
        '    W1  =', ES12.5, '    W2  =', ES12.5, /,                    &
        '   QRAD1=', ES12.5, '   QRAD2=', ES12.5, '    OXR =', ES12.5,  &
        '  DTMIN1=', ES12.5, '  DTMIN2=', ES12.5, /,                    &
        '    DT =', ES12.5, '   DTMIN=', ES12.5, '    TS2 =', ES12.5,   &
        '    TS3 =', ES12.5, '    TS4 =', ES12.5, /)

    continue

    write(unit=ounit, fmt=1)                                            &
        this%T, this%XM,                                                &
        this%TS, this%TGASC, this%TF1, this%TF2, this%TF3,              &
        this%TF4, this%TWC1, this%TWC2, this%TWC3, this%TWC4,           &
        this%QCONV1, this%QCONV2, this%PGASC, this%QROD, this%QRAD3,    &
        this%TS1, this%SUM2, this%OX, this%RHOC, this%SUM1,             &
        this%W, this%F40, this%C, this%W1, this%W2,                     &
        this%QRAD1, this%QRAD2, this%OXR, this%DTMIN1, this%DTMIN2,     &
        this%DT, this%DTMIN, this%TS2, this%TS3, this%TS4

    return
end subroutine result_dataframe_write_legacy_output

!> Write transient results header information in CSV format
subroutine result_dataframe_write_csv_header(this, ounit)
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

! 1   format('"T","OXR","PGASC","TF1","TF2",',                            &
!            '"TF3","TF4","TGASC","TS","TS1",',                           &
!            '"TS2","TS3","TS4","TWC1","TWC2",',                          &
!            '"TWC3","TWC4","XM","QCONV1","QCONV2",',                     &
!            '"QRAD1","QRAD2","QRAD3","QROD","OX",',                      &
!            '"RHOC","SUM1","SUM2","W","F40",',                           &
!            '"C","W1","W2","DTMIN1","DTMIN2",',                          &
!            '"DT","DTMIN"')

1  format('"T", "XM", "TS", "TGASC", "TF1", "TF2", "TF3", "TF4", ',     &
           '"TWC1", "TWC2", "TWC3", "TWC4", "QCONV1", "QCONV2", ',      &
           '"PGASC", "QROD", "QRAD3", "TS1", "SUM2", "OX", "RHOC", ',   &
           '"SUM1", "W", "F40", "C", "W1", "W2", "QRAD1", "QRAD2", ',   &
           '"OXR", "DTMIN1", "DTMIN2", "DT", "DTMIN", "TS2", "TS3",',   &
           ' "TS4"')
    continue

    write(unit=ounit, fmt=1)

    return
end subroutine result_dataframe_write_csv_header

!> Write transient results data in CSV format
subroutine result_dataframe_write_csv_data(this, ounit)
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format(ES12.5, 36(",", ES12.5))

    continue

    ! write(unit=ounit, fmt=1)                                            &
    !     this%T, this%OXR, this%PGASC, this%TF1, this%TF2,               &
    !     this%TF3, this%TF4, this%TGASC, this%TS, this%TS1,              &
    !     this%TS2, this%TS3, this%TS4, this%TWC1, this%TWC2,             &
    !     this%TWC3, this%TWC4, this%XM, this%QCONV1, this%QCONV2,        &
    !     this%QRAD1, this%QRAD2, this%QRAD3, this%QROD, this%OX,         &
    !     this%RHOC, this%SUM1, this%SUM2, this%W, this%F40,              &
    !     this%C, this%W1, this%W2, this%DTMIN1, this%DTMIN2,             &
    !     this%DT, this%DTMIN

    write(unit=ounit, fmt=1)                                            &
        this%T, this%XM, this%TS, this%TGASC, this%TF1, this%TF2,       &
        this%TF3, this%TF4, this%TWC1, this%TWC2, this%TWC3, this%TWC4, &
        this%QCONV1, this%QCONV2, this%PGASC, this%QROD, this%QRAD3,    &
        this%TS1, this%SUM2, this%OX, this%RHOC, this%SUM1, this%W,     &
        this%F40, this%C, this%W1, this%W2, this%QRAD1, this%QRAD2,     &
        this%OXR, this%DTMIN1, this%DTMIN2, this%DT, this%DTMIN,        &
        this%TS2, this%TS3, this%TS4

    return
end subroutine result_dataframe_write_csv_data

!> Write summary results header information in CSV format
subroutine result_dataframe_write_summary_csv_header(this, ounit)
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit
    
1   format('"ST","SPGASC","STF1","STF2","STGASC","STS1","STWC1",',      &
           '"STWC2","SXM"')
    continue

    write(unit=ounit, fmt=1)

    return
end subroutine result_dataframe_write_summary_csv_header

!> Write summary results data in CSV format
subroutine result_dataframe_write_summary_csv_data(this, ounit)
    implicit none
    
    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format(ES12.5, 8(",", ES12.5))

    continue

    write(unit=ounit, fmt=1) this%ST, this%SPGASC, this%STF1,           &
        this%STF2, this%STGASC, this%STS1, this%STWC1, this%STWC2,      &
        this%SXM

    return
end subroutine result_dataframe_write_summary_csv_data

!> Initialize all scenario dataframe elements
subroutine scenario_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none
    
    !> Object reference
    class(scenario_dataframe), intent(inout) :: this

    continue

    this%TITLE = ''

    this%TSI = ZERO
    this%TS1I = ZERO
    this%TS2I = ZERO
    this%TS3I = ZERO
    this%TS4I = ZERO
    this%TF1I = ZERO
    this%TF2I = ZERO
    this%TF3I = ZERO
    this%TF4I = ZERO
    this%TWC1I = ZERO
    this%TWC2I = ZERO
    this%TWC3I = ZERO
    this%TWC4I = ZERO
    this%TGASCI = ZERO
    this%PGASCI = ZERO
    this%TI = ZERO
    this%TA = ZERO
    this%PA = ZERO
    this%XMI = ZERO
    this%XMAX = ZERO
    this%AF = ZERO
    this%A1 = ZERO
    this%A2 = ZERO
    this%A5 = ZERO
    this%CO = ZERO
    this%ANA = ZERO
    this%QC = ZERO
    this%VOLAC = ZERO
    this%AF2 = ZERO
    this%AF3 = ZERO
    this%SOD = ZERO
    this%PRT1 = ZERO
    this%PRT2 = ZERO
    this%PRT3 = ZERO
    this%F3 = ZERO
    this%CON = ZERO
    this%X = ZERO
    this%S1 = ZERO
    this%S2 = ZERO
    this%S3 = ZERO
    this%S4 = ZERO
    this%ZNAS = ZERO
    this%ZNAS1 = ZERO
    this%ZNAS2 = ZERO
    this%ZNAS3 = ZERO
    this%ZNAS4 = ZERO
    this%ZF1 = ZERO
    this%ZF2 = ZERO
    this%ZF3 = ZERO
    this%ZF4 = ZERO
    this%XWC1 = ZERO
    this%XWC2 = ZERO
    this%XWC3 = ZERO
    this%XWC4 = ZERO
    this%CPAC = ZERO
    this%CPS = ZERO
    this%CPF1 = ZERO
    this%CPF2 = ZERO
    this%CPF3 = ZERO
    this%CPF4 = ZERO
    this%CPWC1 = ZERO
    this%CPWC2 = ZERO
    this%CPWC3 = ZERO
    this%CPWC4 = ZERO
    this%AKS = ZERO
    this%AKF1 = ZERO
    this%AKF12 = ZERO
    this%AKF2 = ZERO
    this%AKF3 = ZERO
    this%AKF4 = ZERO
    this%AKWC1 = ZERO
    this%AKWC12 = ZERO
    this%AKWC2 = ZERO
    this%AKWC3 = ZERO
    this%AKWC4 = ZERO
    this%RHOA = ZERO
    this%RHS = ZERO
    this%RHF1 = ZERO
    this%RHF2 = ZERO
    this%RHF3 = ZERO
    this%RHF4 = ZERO
    this%RHWC1 = ZERO
    this%RHWC2 = ZERO
    this%RHWC3 = ZERO
    this%RHWC4 = ZERO
    this%ZF12 = ZERO
    this%XWC12 = ZERO

    return
end subroutine scenario_dataframe_init

!> Set numerical elements from array
subroutine scenario_dataframe_set_from_array(this, PUTINS)
    use iso_fortran_env, only: stderr => error_unit
    use m_constant, only: ZERO
    implicit none
    
! Arguments

    !> Object reference
    class(scenario_dataframe), intent(inout) :: this

    !> Array of input values
    real(kind=WP), dimension(:), intent(in) :: PUTINS

! Local variables

    integer :: NINPUTS

! Formats

500 format('ERROR: Cannot completely specify scenario with short ',     &
        'input array; ', I3, ' elements found, ', I3, ' are ',          &
        'required. Clearing input.')

    continue

    NINPUTS = size(PUTINS, dim=1)
    if (NINPUTS >= this%NPARAMS) then
        this%TSI = PUTINS(1)
        this%TS1I = PUTINS(2)
        this%TS2I = PUTINS(3)
        this%TS3I = PUTINS(4)
        this%TS4I = PUTINS(5)

        this%TF1I = PUTINS(6)
        this%TF2I = PUTINS(7)
        this%TF3I = PUTINS(8)
        this%TF4I = PUTINS(9)
        this%TWC1I = PUTINS(10)

        this%TWC2I = PUTINS(11)
        this%TWC3I = PUTINS(12)
        this%TWC4I = PUTINS(13)
        this%TGASCI = PUTINS(14)
        this%PGASCI = PUTINS(15)

        this%TI = PUTINS(16)
        this%TA = PUTINS(17)
        this%PA = PUTINS(18)
        this%XMI = PUTINS(19)
        this%XMAX = PUTINS(20)

        this%AF = PUTINS(21)
        this%A1 = PUTINS(22)
        this%A2 = PUTINS(23)
        this%A5 = PUTINS(24)
        this%CO = PUTINS(25)

        this%ANA = PUTINS(26)
        this%QC = PUTINS(27)
        this%VOLAC = PUTINS(28)
        this%AF2 = PUTINS(29)
        this%AF3 = PUTINS(30)

        this%SOD = PUTINS(31)
        this%PRT1 = PUTINS(32)
        this%PRT2 = PUTINS(33)
        this%PRT3 = PUTINS(34)
        this%F3 = PUTINS(35)

        this%CON = PUTINS(36)
        this%X = PUTINS(37)
        this%S1 = PUTINS(38)
        this%S2 = PUTINS(39)
        this%S3 = PUTINS(40)

        this%S4 = PUTINS(41)
        this%ZNAS = PUTINS(42)
        this%ZNAS1 = PUTINS(43)
        this%ZNAS2 = PUTINS(44)
        this%ZNAS3 = PUTINS(45)

        this%ZNAS4 = PUTINS(46)
        this%ZF1 = PUTINS(47)
        this%ZF2 = PUTINS(48)
        this%ZF3 = PUTINS(49)
        this%ZF4 = PUTINS(50)

        this%XWC1 = PUTINS(51)
        this%XWC2 = PUTINS(52)
        this%XWC3 = PUTINS(53)
        this%XWC4 = PUTINS(54)
        this%CPAC = PUTINS(55)

        this%CPS = PUTINS(56)
        this%CPF1 = PUTINS(57)
        this%CPF2 = PUTINS(58)
        this%CPF3 = PUTINS(59)
        this%CPF4 = PUTINS(60)

        this%CPWC1 = PUTINS(61)
        this%CPWC2 = PUTINS(62)
        this%CPWC3 = PUTINS(63)
        this%CPWC4 = PUTINS(64)
        this%AKS = PUTINS(65)

        this%AKF1 = PUTINS(66)
        this%AKF12 = PUTINS(67)
        this%AKF2 = PUTINS(68)
        this%AKF3 = PUTINS(69)
        this%AKF4 = PUTINS(70)

        this%AKWC1 = PUTINS(71)
        this%AKWC12 = PUTINS(72)
        this%AKWC2 = PUTINS(73)
        this%AKWC3 = PUTINS(74)
        this%AKWC4 = PUTINS(75)

        this%RHOA = PUTINS(76)
        this%RHS = PUTINS(77)
        this%RHF1 = PUTINS(78)
        this%RHF2 = PUTINS(79)
        this%RHF3 = PUTINS(80)

        this%RHF4 = PUTINS(81)
        this%RHWC1 = PUTINS(82)
        this%RHWC2 = PUTINS(83)
        this%RHWC3 = PUTINS(84)
        this%RHWC4 = PUTINS(85)

        this%ZF12 = PUTINS(86)
        this%XWC12 = PUTINS(87)
    else
        write(unit=stderr, fmt=500) NINPUTS, this%NPARAMS
        call this%init()
    end if

    return
end subroutine scenario_dataframe_set_from_array

end module m_dataframe
