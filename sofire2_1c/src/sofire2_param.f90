!> @file sofire2_param.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical precision, array sizes, and similar
!! modeling constants used by SOFIRE2
!!
!! @note All members are considered public
module sofire2_param
! Alternate method of setting WP to single-precision
!    use iso_fortran_env, only: WP => REAL32
! Alternate method of setting WP to double-precision
!    use iso_fortran_env, only: WP => REAL64
    implicit none

    !> Default real precision (kind). Mnemonic: WP = "working precision"
    ! ! Uncomment for single-precision (default)
    ! integer, parameter :: WP = kind(1.0E0)
    ! Uncomment for double-precision
    integer, parameter :: WP = kind(1.0D0)

    !> Default number of data points in transient results array
    integer, parameter :: NDP = 500

    !> Default size of scenario input array (greater than or equal to
    !! number of individual input parameters)
    integer, parameter :: ND = 120

! contains
end module sofire2_param